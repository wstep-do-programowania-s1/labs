def zadanie(task_num):
    print()
    print("-"*60)
    print(f"Zadanie {task_num}")
    print()


zadanie(2)

def ispermutation(arr1, arr2):
    if len(arr1) != len(arr2):
        return False

    for i in arr1:
        if not i in arr2:
            return False
    
    return True

lst1 = [1, 2, 3, 5, 5]
lst2 = [5, 5, 3, 1, 1]

print(ispermutation(lst1, lst2))

zadanie(3)

songs_rating = {
    "Never gonna give you up": 5,
    "What is love": 4,
    "Hot shit": 6,
    "Mullberry Street": 6,
    "Byłomineło": 5
}

for title, rating in songs_rating.items():
    if rating == 5:
        print(f"{title}: {rating}")

zadanie(4)

def replace(d, v, e):
    for key, value in d.items():
        if value == v:
            d[key] = e

marks = {
    "math": 5,
    "PE": 6,
    "WDTW": 4
}

print(marks)
replace(marks, 4, 5)
print(marks)
replace(marks, 5, 6)
print(marks)

zadanie(5)

def invert(d: dict):
    dInv = dict()

    for item in set(d.values()):
        dInv[item] = []

    for key, value in d.items():
        dInv[value].append(key)

    return dInv

print(invert(songs_rating))

zadanie(6)

def ceasar(alphabet, step):
    code = dict()
    step = step % len(alphabet)
    part1 = alphabet[step:]
    part2 = alphabet[:step] 

    for i in range(len(alphabet) - step):
        code[alphabet[i]] = part1[i]

    for j in range(step):
        code[alphabet[len(alphabet) - step + j]] = part2[j]
    
    return code

alphabet = "aąbcćdeęfghijklłmnńoóprsśtuwyzźż"
code = ceasar(alphabet, 3)
message = "MĘŻNY BĄDŹ, CHROŃ PUŁK TWÓJ I SZEŚĆ FLAG"
new_message = ""

print(code[message[0].lower()])

for i in range(len(message)):
    if message[i] in alphabet:
        new_letter = code[message[i].lower()]
    else:
        new_letter = message[i]
    new_message += new_letter.upper()

print(new_message)