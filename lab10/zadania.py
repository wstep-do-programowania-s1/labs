# import sys

# sys.path.append("../lab10/repo")


# from repo.misc import Misc


def zadanie(task_num):
    print()
    print("-"*60)
    print(f"Zadanie {task_num}")
    print()


zadanie(1)

def silnia(n):
    if n == 1 or n == 0:
        return 1
    else:
        return n * silnia(n-1)

print(silnia(6))

zadanie(2)

def fib(n):
    if n == 0 or n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)
    
print(fib(10))

zadanie(3)

def s(n):
    if n == 1:
        return 1
    else:
        return 1/n + s(n-1)
    
print(s(5))

zadanie(4)

def power2(n):
    if n == 1:
        return 1
    else:
        return power2(n-1) + (n-1) + (n-1) + 1
    
print(power2(8))

zadanie(5)

def even_sum(n):
    if n % 2 == 1:
        n -= 1
    if n == 2:
        return 2
    
    return n + even_sum(n-2)
    
print(even_sum(9))

zadanie(6)

def binary_search(array, x):
    mid = len(array) // 2

    if array[mid] == x:
        return True
    elif array[mid] < x:
        return binary_search(array[mid:], x)
    else:
        pass