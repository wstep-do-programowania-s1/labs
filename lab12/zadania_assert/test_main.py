from main import *
import random
import pytest

sorted_lst = [2, 3, 5, 7, 8, 10, 13, 16]
num1 = 7
num2 = 11

def test1_binary_search():
    assert binary_search(num1, sorted_lst) == (num1 in sorted_lst)

def test2_binary_search():
    for num2 in [0,2,4,17]:
        assert binary_search(num2, sorted_lst) == (num2 in sorted_lst)

random_lst1 = [random.randint(0,100) for _ in range(20)]
random_lst2 = random_lst1 + ["bug"]

def test1_max_sort():
    assert max_sort(random_lst1) == sorted(random_lst1)

def test2_max_sort():
    with pytest.raises(TypeError) as excinfo:
        max_sort(random_lst2)

    assert str(excinfo.value) == "Lista musi zawierać same liczby!"


def test1_bubble_sort():
    assert bubble_sort(random_lst1) == sorted(random_lst1)

def test2_bubble_sort():
    with pytest.raises(TypeError) as excinfo:
        bubble_sort(random_lst2)

    assert str(excinfo.value) == "Lista musi zawierać same liczby!"

def test1_suma():
    assert suma(7, 4) == 7 + 4

def test2_suma():
    assert suma(2, 2) != 5
