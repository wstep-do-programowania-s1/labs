def binary_search(x, lst):
    i = 0
    j = len(lst) - 1
    

    while i <= j:
        m = (i + j) // 2

        if lst[m] == x:
            return True
        elif lst[m] > x:
            j = m - 1
        elif lst[m] < x:
            i = m + 1
    
    return False

def max_sort(lst: list):
    l = len(lst)

    for i in range(len(lst) - 1):
        try:
            max_num = max(lst[0:l-i])
        except TypeError:
            raise TypeError("Lista musi zawierać same liczby!")
        
        max_num_index = lst[0:l-i].index(max_num)

        lst[max_num_index], lst[l-i-1] = lst[l-i-1], lst[max_num_index]
        
    return lst

def bubble_sort(lst):
    n = len(lst)

    for i in range(n):
        for j in range(i, n):
            
            try:
                if lst[i] > lst[j]:
                    lst[j], lst[i] = lst[i], lst[j]
            except TypeError:
                raise TypeError("Lista musi zawierać same liczby!")

    return lst

def suma(a, b):
    return a + b

